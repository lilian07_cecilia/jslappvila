﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace recuperacao
{
    /// <summary>
    /// Lógica interna para FrmCadProduto.xaml
    /// </summary>
    public partial class FrmCadProduto : Window
    {
        string codigo;
        public FrmCadProduto()
        {
            InitializeComponent();
        }

        private void TxtDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtPrecoUnitario.IsEnabled = true;
                TxtPrecoUnitario.Focus();
            }
        }

        private void TxtPrecoUnitario_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtValorTotal.IsEnabled = true;
                TxtValorTotal.Focus();
            }
        }

        private void TxtValorTotal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtQuantidadeEstoque.IsEnabled = true;
                TxtQuantidadeEstoque.Focus();
            }
        }

        private void TxtQuantidadeEstoque_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtDataValidade.IsEnabled = true;
                TxtDataValidade.Focus();
            }
        }

        private void TxtDataValidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CmbFornecedor.IsEnabled = true;
                CmbFornecedor.Focus();
            }
        }

        private void CmbFornecedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM fornecedor WHERE nomeFornecedor = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nomeFornecedor", MySqlDbType.String).Value = CmbFornecedor.Text;
                /*Aqui no CommandType tem que definir se vai atualizar uma Stored Procedure o */
                comm.CommandType = CommandType.Text; /* Executa o comando */
                                                     // recebe o conteúdo do banco 
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigo = dr.GetString(0); // Variavel codigo vai armazenar o ID do Fornecedor!

                BtnLimpar.IsEnabled = true;
                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();
            }
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            TxtDescricao.IsEnabled = true;
            TxtDescricao.Focus();
            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxtDescricao.Clear();
            TxtPrecoUnitario.Clear();
            TxtValorTotal.Clear();
            TxtQuantidadeEstoque.Clear();
            TxtDataValidade.Clear();
            CmbFornecedor.Text = "";

            TxtDescricao.IsEnabled = false;
            TxtPrecoUnitario.IsEnabled = false;
            TxtValorTotal.IsEnabled = false;
            TxtQuantidadeEstoque.IsEnabled = false;
            TxtDataValidade.IsEnabled = false;
            CmbFornecedor.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }
    }
}
