﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace recuperacao
{
    /// <summary>
    /// Lógica interna para FrmCadFunc.xaml
    /// </summary>
    public partial class FrmCadFunc : Window
    {
        string codigo;
        public FrmCadFunc()
        {
            InitializeComponent();
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }
                
        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)

            {
                TxtDataAdmissao.Text = DateTime.Now.ToShortDateString();
                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtNumero.IsEnabled = true;
                TxtNumero.Focus();
            }
        }

        private void TxtNumero_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtComplemento.IsEnabled = true;
                TxtComplemento.Focus();
            }
        }

        private void TxtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtBairro.IsEnabled = true;
                TxtBairro.Focus();
            }
        }

        private void TxtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtCidade.IsEnabled = true;
                TxtCidade.Focus();
            }
        }

        private void TxtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtUf.IsEnabled = true;
                TxtUf.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CmbCargo.IsEnabled = true;
                CmbCargo.Focus();
            }
        }

        private void TxtUf_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void CmbCargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM especialidade WHERE descricao = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@descricao", MySqlDbType.String).Value = CmbCargo.Text;
                /*Aqui no CommandType tem que definir se vai atualizar uma Stored Procedure o */
                comm.CommandType = CommandType.Text; /* Executa o comando */
                                                     // recebe o conteúdo do banco 
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigo = dr.GetString(0); // Variavel codigo vai armazenar o ID do Fornecedor!

                TxtDataDemissao.IsEnabled = true;
                TxtDataDemissao.Focus();
            }
        }

        private void TxtDataDemissao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnLimpar.IsEnabled = true;
                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();
            }
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {

            TxtNome.IsEnabled = true;
            TxtNome.Focus();
            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            DateTime DataAdmissao = DateTime.Today;

            if (BtnSalvar.Content.ToString() == "Salvar")
            {
                Banco bd = new Banco();
                bd.Conectar();

                string inserir = "INSERT INTO funcionario (nomeFunc,endereco,numero,bairro,cidade,complemento,uf,cep,telefone,email,datademissao,dataadmissao,especialidade)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    TxtSenha.Text + "','" +
                    DataAdmissao.ToString("yyyy-MM-dd") + "','" +
                    TxtEndereco.Text + "','" +
                    TxtNumero.Text + "','" +
                    TxtComplemento.Text + "','" +
                    TxtBairro.Text + "','" +
                    TxtCidade.Text + "','" +
                    TxtUf.Text + "','" +
                    TxtCep.Text + "','" +
                    CmbCargo.Text + "','" +
                    TxtDataDemissao.Text + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();

                bd.Desconectar();
                MessageBox.Show("Funcionario(a) cadastrado(a) com sucesso!!!", "Cadastro de Fucnionarios");
                BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
            else if (BtnSalvar.Content.ToString() == "Atualizar")
            {
                Banco bd = new Banco();
                bd.Conectar();
                string alterar = "UPDATE funcionario SET " +
                    "nomeFunc= '" + TxtNome.Text + "'," +
                    "endereco='" + TxtEndereco.Text + "'," +
                    "numero= '" + TxtNumero.Text + "'," +
                    "bairro='" + TxtBairro.Text + "'," +
                    "cidade='" + TxtCidade.Text + "'," +
                    "complemento= '" + TxtComplemento.Text + "'," +
                    "uf='" + TxtUf.Text + "'," +
                    "cep= '" + TxtCep.Text + "'," +
                    "email='" + TxtEmail.Text + "'," +
                    "datademissao='" + TxtDataDemissao.Text + "'," +
                    "dataadmissao='" + TxtDataAdmissao.Text + "'," +
                    "especialidade='" + CmbCargo + "'";

                MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
                comandos.ExecuteNonQuery();
                bd.Desconectar();
                MessageBox.Show("Funcionario(a) alterado(a) com sucesso!!!");
                BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

                bd.Desconectar();
                MessageBox.Show("Funcionario(a) cadastrado(a) com sucesso!!!", "Cadastro de Funcionarios");
                BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

                var tel = MessageBox.Show(
                    "Deseja inderir um numero de telefone para esse(a) funcionario(a)?",
                    "Cadastro Telefone",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Exclamation);

                if (tel != MessageBoxResult.Yes)
                {
                    return;
                }
                else
                {
                    MainWindow frm = new MainWindow();
                    frm.Show();

                    this.Close();
                }
            }
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.Clear();
            TxtEmail.Clear();
            TxtDataAdmissao.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            CmbCargo.Text = "";
            TxtDataDemissao.Clear();


            TxtNome.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtDataAdmissao.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = false;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            CmbCargo.IsEnabled = false;
            TxtDataDemissao.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }

        private void BtnAlterar_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.IsEnabled = true;
            TxtEmail.IsEnabled = true;
            TxtDataAdmissao.IsEnabled = true;
            TxtEndereco.IsEnabled = true;
            TxtNumero.IsEnabled = true;
            TxtComplemento.IsEnabled = true;
            TxtBairro.IsEnabled = true;
            TxtCidade.IsEnabled = true;
            TxtUf.IsEnabled = true;
            TxtCep.IsEnabled = true;
            CmbCargo.IsEnabled = true;
            TxtDataDemissao.IsEnabled = true;
            BtnSalvar.IsEnabled = true;
            BtnSalvar.Content = "Atualizar";
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            MainWindow frm = new MainWindow();
            frm.Show();
            this.Close();
        }
    }
}
